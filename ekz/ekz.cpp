// ekz.cpp: определяет точку входа для консольного приложения.
//


#include "stdafx.h" 
#include <iostream> 
#include <fstream> 
#include <string>
#include "openssl/conf.h"
#include "openssl/evp.h" 
#include "openssl/err.h" 
#include "openssl/aes.h"

using namespace std;

class dannie {
private:
	string surname;
	string name;
	string number_passport;

public:
	bool full;//по умолчанию имеет значение false

	void vvod() {//функция ввода
		
		cout << "Vvedite vashu familiyu : ";
		cin >> surname;

		cout << "Vvedite vashe imya : ";
		cin >> name;

		cout << "Vvedite vash nomer passporta : ";
		cin >> number_passport;
	};

	string tostring() {//возвращаемая функция, описывает формат записи в файл
		return surname + " " + name + " " + number_passport + "\n";
	};


};


int main()
{
	int lenght;

	unsigned char *key = (unsigned char *)"0123456789"; //ключ и вектор 
	unsigned char *iv = (unsigned char *)"01234567890123456789";

	//расшифрование файла 
	fstream f0, f_decrypted;
	f0.open("dannie_encrypted.txt", std::fstream::in | std::fstream::binary);// йфайл с исходными данными 
	f_decrypted.open("text.txt", std::fstream::out | std::fstream::trunc | std::fstream::binary);//файл для зашифрованных данных

	char buffer[256] = { 0 };
	char out_buffer[256] = { 0 };

	/*1. Создаётся указатель на несуществующую структуру (сама структура ещё не заполнена)
	структура - тип данных в с++, как класс, различия минимальны*/

	EVP_CIPHER_CTX *ctx;
	
	/*2.Для указателя создаётся объект , на который он будет указан*/
	
	ctx = EVP_CIPHER_CTX_new(); //создание структуры с настройками метода 

								/*3.Структура evp_cipher_ctx заполняется настройками*/
	EVP_EncryptInit_ex(ctx, //ссылка на объект.структуру , куда заносятся параметры 
		EVP_aes_256_cbc(), //ссылка на шифр ядро aes 256 (функцию алгоритма) 
		NULL,
		key,//ключ \пароль 
		iv);// рандомайзер 
	
	lenght = 0;
	f0.read(buffer, 256);
	while (f0.gcount() > 0)
	{

		EVP_DecryptUpdate(ctx,
			(unsigned char *)out_buffer,
			&lenght,
			(unsigned char *)buffer,
			f0.gcount());

		f_decrypted.write(out_buffer, lenght);
		f0.read(buffer, 256);
		EVP_DecryptFinal_ex(ctx, (unsigned char*)out_buffer, &lenght);
		f_decrypted.write(out_buffer, lenght);
		f_decrypted.close();
		f0.close();
	}

	while (true) {
		cout << "1.Add new person" << endl;
		cout << "2.All information" << endl;
		cout << "3.Exit" << endl;
		int choice;
		cin >> choice;
		dannie p;
		if (choice == 1) {
			p.vvod();
			ofstream file;//открытие файла для записи в него
			file.open("text.txt", std::fstream::app);
			file << p.tostring();
			file.close();
			 }
		else if (choice == 2) {
			ifstream file;//открытие файла для считывания информации из него
			file.open("text.txt");
			string tmp;
			cout << endl;
			while (getline(file, tmp)) {
				cout << tmp << endl;
			}
			cout << endl;
			file.close();
			
		}
		else if (choice == 3) {

			break;
		}
		else {
			cout << "Error" << endl;
		}
	}
	//Шифрование файлов 
	//Создаются 2 файловых потока , временные буферы чтения записи , буфер для зашифрованных данных 
	//Цикл while работающий пока в файле остались не прочитанные данные в цикле : считыванние очередной порции из файла , шифрование порции 
	//запись шифра в другой файл 
	fstream f1, f_encrypted;
	f1.open("text.txt", std::fstream::in | std::fstream::binary);// йфайл с исходными данными 
	f_encrypted.open("dannie_encrypted.txt", std::fstream::out | std::fstream::trunc | std::fstream::binary);	//файл для зашифрованных данных

	EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
	f1.read(buffer, 256);
	while (f1.gcount() > 0)
	{

		EVP_EncryptUpdate(ctx,
			(unsigned char *)out_buffer,
			&lenght,
			(unsigned char *)buffer,
			f1.gcount());
		f_encrypted.write(out_buffer, lenght);


		f1.read(buffer, 256);

		EVP_EncryptFinal_ex(ctx, (unsigned char*)out_buffer, &lenght);
		f_encrypted.write(out_buffer, lenght);
		f_encrypted.close();
		f1.close();
	}


	getchar(); 
	return 0;


};

